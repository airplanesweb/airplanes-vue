import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

import createPersistedState from 'vuex-persistedstate'

export default new Vuex.Store({
  state: {
    flights: [],
    updateDatesEvent: undefined,
    filters: {
      withoutBaggage: false,
      minPrice: 0,
      maxPrice: 500000,
      allCompanies: true
    },
    search: {
      departAirport: '',
      departAirportID: undefined,
      departDate: undefined,

      arriveAirport: '',
      arriveAirportID: undefined,
      arriveDate: undefined,
      comfortClass: 0
    },
    tickets: []
  },
  getters: {
    filtteredFlights: (state) => {
      let flights = state.flights

      if(state.filters.withoutBaggage){
        flights = flights.filter(flight => {
          if(flight.baggage === false) return true
        })
      }

      flights = flights.filter(flight => {
        if(flight.price >= state.filters.minPrice &&
           flight.price <= state.filters.maxPrice) {
          return true
        }
      })

      return flights

    }
  },
  plugins: [
    createPersistedState()
  ],
  mutations: {
    setState(state, {type, value}){
      state[type] = value
    },
    setStateProp(state, {type, prop, value}) {
      state[type][prop] = value
    },
    setFilter(state, {type, value}){
      state.filters[type] = value
    }
  },
  actions: {
    async fetchFlights(context){

      const params = {
        depart_id: context.state.search.departAirportID,
        arrive_id: context.state.search.arriveAirportID,
        depart_date: context.state.search.departDate,
        arrive_date: context.state.search.arriveDate,
        comfort_class: context.state.search.comfortClass
      }

      let response = await axios.get('/flights/', {
        params  
      })

      await context.commit('setState', {
        type: 'flights',
        value: response.data
      })

      return true
    }
  }
})
