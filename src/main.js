import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
import store from './store'

let baseURL = ''
if(process.env.NODE_ENV !== 'production'){
  baseURL = 'http://localhost:8000'
}

axios.defaults.baseURL = baseURL
Vue.config.productionTip = false
Vue.prototype.$http = axios
Vue.prototype.$baseURL = baseURL

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
